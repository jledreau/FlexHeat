**Welcome to FlexHeat**

This project provides tools for interactive visualisation of flexibility from space heating :
* a downward modulation, with a decrease of the temperature set-point from 21°C down to 19°C between 6 - 9 pm;
* an upward modulation before 6 pm, with an increase of the temperature set-point during the afternoon (during a few hours).

Different case-studies are available :
*  BR 1982 / BR 2005 / BR 2020
*  radiators / underfloor heating



**Preview**

Downward modulation

![](image/image_down.png)

Upward modulation

![](image/image_up.png)


**Release history**
*  v0 : 14/05/2019
*  v0.1 : 06/06/2019 (preview + update date)


**License**

Licensed by La Rochelle University/LaSIE under a BSD 3 license.



**References**

Le Dréau J., Mellas I., Vellei M., Meulemans J., Upscaling the flexibility potential of space heating in single-family houses, In: CISBAT 2019, Switzerland, 2019.


